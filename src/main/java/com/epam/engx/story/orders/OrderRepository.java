package com.epam.engx.story.orders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class OrderRepository {

  private final Map<Customer, List<Order>> customerOrders = new HashMap<>();

  public void save(Customer customer, Order order) {
    customerOrders.computeIfAbsent(customer, c -> new ArrayList<>()).add(order);
  }

  public List<Order> getOrdersByCustomer(Customer customer) {
    return customerOrders.getOrDefault(customer, Collections.emptyList());
  }
}
