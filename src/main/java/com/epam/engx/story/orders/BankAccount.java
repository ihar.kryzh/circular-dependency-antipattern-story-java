package com.epam.engx.story.orders;

import java.math.BigDecimal;

public class BankAccount {
  private BigDecimal balance = BigDecimal.ZERO;

  public void withdrawOrderAmount(Order order) {
    balance = balance.subtract(order.getAmount());
  }

  public BigDecimal getBalance() {
    return balance;
  }
}
