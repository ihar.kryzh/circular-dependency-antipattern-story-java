package com.epam.engx.story.orders;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class BankAccountsRepository {
  private final Map<Customer, BankAccount> accounts = new HashMap<>();

  public BankAccount getCustomerAccount(Customer customer) {
    accounts.putIfAbsent(customer, new BankAccount());
    return accounts.get(customer);
  }
}
