package com.epam.engx.story.orders;

import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrdersService {

  private final OrderRepository orderRepository;
  private final BankAccountsRepository bankAccountsRepository;

  @Autowired
  public OrdersService(
      OrderRepository orderRepository, BankAccountsRepository bankAccountsRepository) {
    this.orderRepository = orderRepository;
    this.bankAccountsRepository = bankAccountsRepository;
  }

  public Order createOrder(Customer customer, BigDecimal amount) {
    Order order = Order.create(customer, amount);

    processOrder(customer, order);

    orderRepository.save(customer, order);

    return order;
  }

  public void processOrder(Customer customer, Order order) {
    bankAccountsRepository.getCustomerAccount(customer).withdrawOrderAmount(order);
  }
}