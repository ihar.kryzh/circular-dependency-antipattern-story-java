package com.epam.engx.story.orders;

import java.math.BigDecimal;

public class Order {
  private final Customer customer;
  private final BigDecimal amount;

  private Order(Customer customer, BigDecimal amount) {
    this.customer = customer;
    this.amount = amount;
  }

  public static Order create(Customer customer, BigDecimal amount) {
    return new Order(customer, amount);
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public Customer getCustomer() {
    return customer;
  }
}
